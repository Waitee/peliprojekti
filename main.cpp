#include <allegro5/allegro5.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_color.h>
#include <allegro5/allegro_ttf.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>

bool done;
ALLEGRO_EVENT_QUEUE* event_queue;
ALLEGRO_TIMER* timer;
ALLEGRO_DISPLAY* display;
ALLEGRO_BITMAP* image;
ALLEGRO_FONT* maintext;

class character {
    public:
    int x,y;
    int hp, energy;
    ALLEGRO_BITMAP* pic[8];
};


void abort_game(const char* message)
{
    printf("%s \n", message);
    exit(1);
}

void init(void)
{
    //törkeästi initialisaatioita ja checkejä että onnistuu
    if (!al_init())
        abort_game("Failed to initialize allegro");

    if (!al_install_keyboard())
        abort_game("Failed to install keyboard");

    if (!al_init_image_addon())
        abort_game("Failed to init image addon");

    timer = al_create_timer(1.0 / 60);
    if (!timer)
        abort_game("Failed to create timer");

    al_set_new_display_flags(ALLEGRO_WINDOWED);
    display = al_create_display(800, 600);
    if (!display)
        abort_game("Failed to create display");

    al_init_font_addon();
    al_init_ttf_addon();

    event_queue = al_create_event_queue();
    if (!event_queue)
        abort_game("Failed to create event queue");

    maintext = al_load_font("font.ttf", 18, 0);

    //rekisteröidään tapahtumalähteet inputtia varten
    al_register_event_source(event_queue, al_get_keyboard_event_source());
    al_register_event_source(event_queue, al_get_timer_event_source(timer));
    al_register_event_source(event_queue, al_get_display_event_source(display));

    done = false;
}

void shutdown(void)
{
    if (timer)
        al_destroy_timer(timer);

    if (display)
        al_destroy_display(display);

    if (event_queue)
        al_destroy_event_queue(event_queue);
}

void game_loop(void)
{
    //testihahmo
    character test;
    test.x = 101;
    test.y = 150;
    test.pic[0] = al_load_bitmap("gfx/player.png");
    test.hp = 100;
    test.energy = 100;

    bool redraw = true;
    al_start_timer(timer);

    while (!done) {
        ALLEGRO_EVENT event;
        al_wait_for_event(event_queue, &event);
        //ajastimen mukaan tapahtuvat asiat
        if (event.type == ALLEGRO_EVENT_TIMER) {
            redraw = true;
            //update_logic();
        }
        //näppäimistöinput
        else if (event.type == ALLEGRO_EVENT_KEY_DOWN) {
            if (event.keyboard.keycode == ALLEGRO_KEY_ESCAPE) {
                done = true;
            }
            if (event.keyboard.keycode == ALLEGRO_KEY_DOWN)
                test.y += 2;
            if (event.keyboard.keycode == ALLEGRO_KEY_UP)
                test.y -= 2;
            if (event.keyboard.keycode == ALLEGRO_KEY_RIGHT)
                test.x += 2;
            if (event.keyboard.keycode == ALLEGRO_KEY_LEFT)
                test.x -= 2;
            //get_user_input();
        }
        //piirtofunktiot
        if (redraw && al_is_event_queue_empty(event_queue)) {
            redraw = false;
            al_clear_to_color(al_map_rgb(0, 0, 0));
            //update_graphics();
            al_draw_bitmap(test.pic[0],test.x,test.y,0);
            al_draw_text(maintext,al_map_rgb(255,255,255),200,200,0,"Use arrows to move");
            al_flip_display();
        }
    }
}

int main(int argc, char* argv[])
{
    //pääpeli
    init();
    game_loop();
    shutdown();
}
